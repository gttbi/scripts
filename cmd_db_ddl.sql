CREATE MATERIALIZED VIEW dwh_buffer.mv_dwh_buffer_ticket
TABLESPACE pg_default
AS SELECT sync_buffer.sync_log_id,
    sync_buffer.table_row_id AS rowid,
    (sync_buffer.data ->> 'status'::text)::integer AS status,
    (sync_buffer.data ->> 'created_by'::text)::integer AS created_by,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'created_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'created_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'created_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS created_on,
    (sync_buffer.data ->> 'last_updated_by'::text)::integer AS last_updated_by,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'last_updated_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'last_updated_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'last_updated_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS last_updated_on,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'closed_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'closed_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'closed_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS closed_on,
    (sync_buffer.data ->> 'closed_by'::text)::integer AS closed_by,
    ((sync_buffer.data ->> 'client_ticket'::text))::character varying(255) AS client_ticket,
    (sync_buffer.data ->> 'rebate'::text)::integer AS rebate,
    (sync_buffer.data ->> 'outage_total'::text)::integer AS outage_total,
    (sync_buffer.data ->> 'category'::text)::integer AS category,
    (sync_buffer.data ->> 'priority'::text)::integer AS priority,
    (sync_buffer.data ->> 'severity'::text)::integer AS severity,
    (sync_buffer.data ->> 'client_id'::text)::integer AS client_id,
    (sync_buffer.data ->> 'rfo'::text)::integer AS rfo,
    (sync_buffer.data ->> 'vendor_id'::text)::integer AS vendor_id,
    (sync_buffer.data ->> 'intrusive_testing'::text)::integer AS intrusive_testing,
    ((sync_buffer.data ->> 'phone_number'::text))::character varying(255) AS phone_number,
    (sync_buffer.data ->> 'assigned_to'::text)::integer AS assigned_to,
    ((sync_buffer.data ->> 'supplier_ticket_numbers'::text))::character varying(255) AS supplier_ticket_numbers,
    (sync_buffer.data ->> 'fault_report_required'::text)::integer AS fault_report_required,
    (sync_buffer.data ->> 'maintenance_completed'::text)::integer AS maintenance_completed,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'fault_occured'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'fault_occured'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'fault_occured'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS fault_occured,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'service_restored'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'service_restored'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'service_restored'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS service_restored,
    ((sync_buffer.data ->> 'heat_legacy_id'::text))::character varying(255) AS heat_legacy_id,
    (sync_buffer.data ->> 'suspended_minutes'::text)::integer AS suspended_minutes,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'fault_report_sent'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'fault_report_sent'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'fault_report_sent'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS fault_report_sent,
    (sync_buffer.data ->> 'trouble_segment'::text)::integer AS trouble_segment,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'due_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'due_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'due_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS due_date,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'pw_start'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'pw_start'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'pw_start'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS pw_start,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'pw_end'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'pw_end'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'pw_end'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS pw_end,
    ((sync_buffer.data ->> 'pw_duration'::text))::character varying(255) AS pw_duration,
    regexp_replace(sync_buffer.data ->> 'pw_reason'::text, '[^a-zA-Z0-9_._,_|_/_\_ ]+'::text, ''::text, 'g'::text)::character varying(510) AS pw_reason,
    ((sync_buffer.data ->> 'outage_duration'::text))::character varying(255) AS outage_duration,
    ((sync_buffer.data ->> 'customer_contact'::text))::character varying(255) AS customer_contact,
    ((sync_buffer.data ->> 'customer_email'::text))::character varying(255) AS customer_email,
    ((sync_buffer.data ->> 'supplier_contact'::text))::character varying(255) AS supplier_contact,
    ((sync_buffer.data ->> 'supplier_email'::text))::character varying(255) AS supplier_email,
    (sync_buffer.data ->> 'ticket_escalation_level'::text)::integer AS ticket_escalation_level,
    ((sync_buffer.data ->> 'es_negotiation'::text))::character varying(255) AS es_negotiation,
    ((sync_buffer.data ->> 'es_interface_status'::text))::character varying(255) AS es_interface_status,
    ((sync_buffer.data ->> 'ip_bs_router_status'::text))::character varying(255) AS ip_bs_router_status,
    ((sync_buffer.data ->> 'ip_bs_default_gateway'::text))::character varying(255) AS ip_bs_default_gateway,
    ((sync_buffer.data ->> 'ip_bs_reset'::text))::character varying(255) AS ip_bs_reset,
    ((sync_buffer.data ->> 'ip_bs_config_change'::text))::character varying(255) AS ip_bs_config_change,
    ((sync_buffer.data ->> 'cc_pp_frequency'::text))::character varying(255) AS cc_pp_frequency,
    ((sync_buffer.data ->> 'cc_pp_power_verify'::text))::character varying(255) AS cc_pp_power_verify,
    ((sync_buffer.data ->> 'cc_pp_reset'::text))::character varying(255) AS cc_pp_reset,
    ((sync_buffer.data ->> 'cc_pp_cable_check'::text))::character varying(255) AS cc_pp_cable_check,
    ((sync_buffer.data ->> 'cc_pp_any_changes'::text))::character varying(255) AS cc_pp_any_changes,
    ((sync_buffer.data ->> 'cc_pp_visibility'::text))::character varying(255) AS cc_pp_visibility,
    ((sync_buffer.data ->> 'supplier_contact_2'::text))::character varying(255) AS supplier_contact_2,
    ((sync_buffer.data ->> 'supplier_email_2'::text))::character varying(255) AS supplier_email_2,
    ((sync_buffer.data ->> 'supplier_ticket_numbers_2'::text))::character varying(255) AS supplier_ticket_numbers_2,
    ((sync_buffer.data ->> 'supplier_contact_3'::text))::character varying(255) AS supplier_contact_3,
    ((sync_buffer.data ->> 'supplier_email_3'::text))::character varying(255) AS supplier_email_3,
    ((sync_buffer.data ->> 'supplier_ticket_numbers_3'::text))::character varying(255) AS supplier_ticket_numbers_3,
    ((sync_buffer.data ->> 'supplier_contact_4'::text))::character varying(255) AS supplier_contact_4,
    ((sync_buffer.data ->> 'supplier_email_4'::text))::character varying(255) AS supplier_email_4,
    ((sync_buffer.data ->> 'supplier_ticket_numbers_4'::text))::character varying(255) AS supplier_ticket_numbers_4,
    ((sync_buffer.data ->> 'contact_email'::text))::character varying(255) AS contact_email,
    (sync_buffer.data ->> 'basic_check'::text)::integer AS basic_check,
    (sync_buffer.data ->> 'end_test'::text)::integer AS end_test,
    (sync_buffer.data ->> 'head_test'::text)::integer AS head_test,
    (sync_buffer.data ->> 'trace_test'::text)::integer AS trace_test,
    (sync_buffer.data ->> 'vendor_score_id'::text)::integer AS vendor_score_id,
    ((sync_buffer.data ->> 'poc_a_end_contact'::text))::character varying(255) AS poc_a_end_contact,
    ((sync_buffer.data ->> 'poc_a_end_email'::text))::character varying(255) AS poc_a_end_email,
    ((sync_buffer.data ->> 'poc_z_end_contact'::text))::character varying(255) AS poc_z_end_contact,
    ((sync_buffer.data ->> 'poc_z_end_email'::text))::character varying(255) AS poc_z_end_email,
    (sync_buffer.data ->> 'cc_pp_hard_down'::text)::integer AS cc_pp_hard_down,
    (sync_buffer.data ->> 'cc_pp_errors'::text)::integer AS cc_pp_errors,
    (sync_buffer.data ->> 'cc_pp_cct'::text)::integer AS cc_pp_cct,
    ((sync_buffer.data ->> 'cc_pp_a_end'::text))::character varying(255) AS cc_pp_a_end,
    ((sync_buffer.data ->> 'cc_pp_z_end'::text))::character varying(255) AS cc_pp_z_end,
    ((sync_buffer.data ->> 'e1_ts_used_speed'::text))::character varying(255) AS e1_ts_used_speed,
    (sync_buffer.data ->> 'e1_crc_setting'::text)::integer AS e1_crc_setting,
    ((sync_buffer.data ->> 'e1_clock_setting'::text))::character varying(255) AS e1_clock_setting,
    (sync_buffer.data ->> 'es_duplex'::text)::integer AS es_duplex,
    (sync_buffer.data ->> 'auto_neg'::text)::integer AS auto_neg,
    ((sync_buffer.data ->> 'equipment_a_end'::text))::character varying(255) AS equipment_a_end,
    ((sync_buffer.data ->> 'equipment_z_end'::text))::character varying(255) AS equipment_z_end,
    ((sync_buffer.data ->> 'alarms_a_end'::text))::character varying(255) AS alarms_a_end,
    ((sync_buffer.data ->> 'alarms_z_end'::text))::character varying(255) AS alarms_z_end,
    ((sync_buffer.data ->> 'visibility_a_end'::text))::character varying(255) AS visibility_a_end,
    ((sync_buffer.data ->> 'visibility_z_end'::text))::character varying(255) AS visibility_z_end,
    ((sync_buffer.data ->> 'p_e_verified_a_end'::text))::character varying(255) AS p_e_verified_a_end,
    ((sync_buffer.data ->> 'p_e_verified_z_end'::text))::character varying(255) AS p_e_verified_z_end,
    ((sync_buffer.data ->> 'interface_reset_a_end'::text))::character varying(255) AS interface_reset_a_end,
    ((sync_buffer.data ->> 'interface_reset_z_end'::text))::character varying(255) AS interface_reset_z_end,
    ((sync_buffer.data ->> 'cabling_checked_a_end'::text))::character varying(255) AS cabling_checked_a_end,
    ((sync_buffer.data ->> 'cabling_checked_z_end'::text))::character varying(255) AS cabling_checked_z_end,
    ((sync_buffer.data ->> 'modem_status_a_end'::text))::character varying(255) AS modem_status_a_end,
    ((sync_buffer.data ->> 'modem_status_z_end'::text))::character varying(255) AS modem_status_z_end,
    (sync_buffer.data ->> 'subcategory'::text)::integer AS subcategory,
    (sync_buffer.data ->> 'problem1_status'::text)::integer AS problem1_status,
    (sync_buffer.data ->> 'problem2_status'::text)::integer AS problem2_status,
    (sync_buffer.data ->> 'problem3_status'::text)::integer AS problem3_status,
    (sync_buffer.data ->> 'ticket_queue_id'::text)::integer AS ticket_queue_id,
    (sync_buffer.data ->> 'linked_ticket_id'::text)::integer AS linked_ticket_id,
    (sync_buffer.data ->> 'master_ticket'::text)::integer AS master_ticket,
    (sync_buffer.data ->> 'master_ticket_id'::text)::integer AS master_ticket_id,
    (sync_buffer.data ->> 'alert_level'::text)::integer AS alert_level,
    (sync_buffer.data ->> 'last_ticket_entry'::text)::integer AS last_ticket_entry,
    ((sync_buffer.data ->> 'carbon_copy'::text))::character varying(510) AS carbon_copy,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'customer_close_call'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'customer_close_call'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'customer_close_call'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS customer_close_call,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'customer_hold_call'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'customer_hold_call'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'customer_hold_call'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS customer_hold_call,
    (sync_buffer.data ->> 'customer_escalation'::text)::integer AS customer_escalation,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'p1_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'p1_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'p1_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS p1_scheduled_date,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'p2_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'p2_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'p2_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS p2_scheduled_date,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'p3_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'p3_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'p3_scheduled_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS p3_scheduled_date,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'pw_start_backup1'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'pw_start_backup1'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'pw_start_backup1'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS pw_start_backup1,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'pw_end_backup1'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'pw_end_backup1'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'pw_end_backup1'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS pw_end_backup1,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'pw_start_backup2'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'pw_start_backup2'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'pw_start_backup2'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS pw_start_backup2,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'pw_end_backup2'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'pw_end_backup2'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'pw_end_backup2'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS pw_end_backup2,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'survey_sent_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'survey_sent_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'survey_sent_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS survey_sent_date,
    (sync_buffer.data ->> 'root_cause_id'::text)::integer AS root_cause_id,
    (sync_buffer.data ->> 'suppress_notifications'::text)::integer AS suppress_notifications,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'next_action_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'next_action_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'next_action_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS next_action_date,
    (sync_buffer.data ->> 'customer_hold_time'::text)::integer AS customer_hold_time,
    (sync_buffer.data ->> 'telco_hold_time'::text)::integer AS telco_hold_time,
    (sync_buffer.data ->> 'ticket_source_id'::text)::integer AS ticket_source_id,
    ((sync_buffer.data ->> 'mttr'::text))::numeric(18,6) AS mttr,
    ((sync_buffer.data ->> 'customer_severity_level'::text))::numeric(18,6) AS customer_severity_level,
    (sync_buffer.data ->> 'technology_id'::text)::integer AS technology_id,
    (sync_buffer.data ->> 'ticket_rfo_group_id'::text)::integer AS ticket_rfo_group_id,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'msa_change_request_start_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'msa_change_request_start_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'msa_change_request_start_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS msa_change_request_start_date,
    (sync_buffer.data ->> 'msa_primary_contact'::text)::integer AS msa_primary_contact,
    (sync_buffer.data ->> 'msa_is_ticket_visible'::text)::integer AS msa_is_ticket_visible,
    (sync_buffer.data ->> 'import_source'::text)::integer AS import_source,
    ((sync_buffer.data ->> 'legacy_ticket_id'::text))::character varying(50) AS legacy_ticket_id,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'answering_service_initial_email_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'answering_service_initial_email_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'answering_service_initial_email_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS answering_service_initial_email_time,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'answering_service_customer_contact_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'answering_service_customer_contact_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'answering_service_customer_contact_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS answering_service_customer_contact_time,
    ((sync_buffer.data ->> 'sears_incident_id'::text))::character varying(255) AS sears_incident_id,
    (sync_buffer.data ->> 'ticket_sears_fault_cause_id'::text)::integer AS ticket_sears_fault_cause_id,
    ((sync_buffer.data ->> 'ticket_sears_api_response_value'::text))::character varying(50) AS ticket_sears_api_response_value,
    (sync_buffer.data ->> 'is_automatic_maintenance_notification_sent'::text)::integer AS is_automatic_maintenance_notification_sent,
    ((sync_buffer.data ->> 'historical_ticket_attachments_url'::text))::character varying(500) AS historical_ticket_attachments_url,
    (sync_buffer.data ->> 'customer_impacting'::text)::integer AS customer_impacting,
    (sync_buffer.data ->> 'ict_ticket_id'::text)::integer AS ict_ticket_id,
    (sync_buffer.data ->> 'next_action_date_notification_counter'::text)::integer AS next_action_date_notification_counter,
    ((sync_buffer.data ->> 'mrr_increase'::text))::numeric(18,6) AS mrr_increase,
    (sync_buffer.data ->> 'ticket_hold_on_substatus_id'::text)::integer AS ticket_hold_on_substatus_id,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'dispatch_confirmation_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'dispatch_confirmation_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'dispatch_confirmation_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS dispatch_confirmation_date,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'dispatch_start_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'dispatch_start_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'dispatch_start_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS dispatch_start_time,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'dispatch_end_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'dispatch_end_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'dispatch_end_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS dispatch_end_time,
    (sync_buffer.data ->> 'dispatch_pi_vendor_id'::text)::integer AS dispatch_pi_vendor_id,
    ((sync_buffer.data ->> 'dispatch_pi_vendor_order_number'::text))::character varying(50) AS dispatch_pi_vendor_order_number,
    (sync_buffer.data ->> 'dispatch_approved_by_id'::text)::integer AS dispatch_approved_by_id,
    ((sync_buffer.data ->> 'dispatch_additional_cost'::text))::numeric(18,6) AS dispatch_additional_cost,
    (sync_buffer.data ->> 'dispatch_additional_cost_currency'::text)::integer AS dispatch_additional_cost_currency,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'dispatch_technician_arrival_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'dispatch_technician_arrival_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'dispatch_technician_arrival_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS dispatch_technician_arrival_time,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'dispatch_technician_departure_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'dispatch_technician_departure_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'dispatch_technician_departure_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS dispatch_technician_departure_time,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'dispatch_actual_pi_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'dispatch_actual_pi_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'dispatch_actual_pi_date'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS dispatch_actual_pi_date,
    (sync_buffer.data ->> 'dispatch_description'::text)::integer AS dispatch_description,
    (sync_buffer.data ->> 'dispatch_is_billable'::text)::integer AS dispatch_is_billable,
    (sync_buffer.data ->> 'dispatch_pi_vendor_order_id'::text)::integer AS dispatch_pi_vendor_order_id,
    (sync_buffer.data ->> 'is_worked_ticket'::text)::integer AS is_worked_ticket,
    ((sync_buffer.data ->> 'vendor_downtime'::text))::numeric(18,6) AS vendor_downtime,
    (sync_buffer.data ->> 'is_processed'::text)::integer AS is_processed,
    (sync_buffer.data ->> 'rma_automation_status_id'::text)::integer AS rma_automation_status_id,
    (sync_buffer.data ->> 'client_billing_account_id'::text)::integer AS client_billing_account_id,
    (sync_buffer.data ->> 'access_hours_start'::text)::integer AS access_hours_start,
    (sync_buffer.data ->> 'access_hours_end'::text)::integer AS access_hours_end,
    (sync_buffer.data ->> 'access_hours_weekend_start'::text)::integer AS access_hours_weekend_start,
    (sync_buffer.data ->> 'access_hours_weekend_end'::text)::integer AS access_hours_weekend_end,
    (sync_buffer.data ->> 'access_hours_occurrence'::text)::integer AS access_hours_occurrence,
    (sync_buffer.data ->> 'access_hours_timezone'::text)::integer AS access_hours_timezone,
    ((sync_buffer.data ->> 'next_action_manual_set'::text))::character varying(5) AS next_action_manual_set,
    ((sync_buffer.data ->> 'dispatch_mileage'::text))::character varying(255) AS dispatch_mileage,
    ((sync_buffer.data ->> 'dispatch_cpe_model'::text))::character varying(255) AS dispatch_cpe_model,
    ((sync_buffer.data ->> 'dispatch_cpe_serial_number'::text))::character varying(255) AS dispatch_cpe_serial_number,
    (sync_buffer.data ->> 'dispatch_is_cpe_replaced'::text)::integer AS dispatch_is_cpe_replaced,
    ((sync_buffer.data ->> 'dispatch_cpe_password'::text))::character varying(255) AS dispatch_cpe_password,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'dispatch_travel_start_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'dispatch_travel_start_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'dispatch_travel_start_time'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS dispatch_travel_start_time,
    (sync_buffer.data ->> 'dispatch_dart_resolution_code_id'::text)::integer AS dispatch_dart_resolution_code_id,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'purchase_request_submitted_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'purchase_request_submitted_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'purchase_request_submitted_on'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS purchase_request_submitted_on,
    (sync_buffer.data ->> 'client_segment_historical_id'::text)::integer AS client_segment_historical_id,
    (sync_buffer.data ->> 'disable_access_hours'::text)::integer AS disable_access_hours,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'date_created'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'date_created'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'date_created'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS date_created,
        CASE
            WHEN to_timestamp(sync_buffer.data ->> 'date_modified'::text, 'YYYY-MM-DD hh24:MI:SS'::text) < '1900-01-01 00:00:00'::timestamp without time zone OR to_timestamp(sync_buffer.data ->> 'date_modified'::text, 'YYYY-MM-DD hh24:MI:SS'::text) > '2400-01-01 00:00:00'::timestamp without time zone THEN '1900-01-01 00:00:00'::timestamp without time zone::timestamp with time zone
            ELSE to_timestamp(sync_buffer.data ->> 'date_modified'::text, 'YYYY-MM-DD hh24:MI:SS'::text)
        END AS date_modified,
    (sync_buffer.data ->> 'incident_manager_id'::text)::integer AS incident_manager_id,
    (sync_buffer.data ->> 'approval_status_id'::text)::integer AS approval_status_id,
    (sync_buffer.data ->> 'impact_security'::text)::integer AS impact_security,
    ((sync_buffer.data ->> 'internal_MTTR'::text))::character varying(50) AS internal_mttr,
    (sync_buffer.data ->> 'closure_quality'::text)::integer AS closure_quality
   FROM dwh_buffer.sync_buffer
  WHERE sync_buffer.table_name::text = 'ticket'::text AND COALESCE(sync_buffer.processed::integer, 0) = 0 AND sync_buffer.sync_log_id = (( SELECT min(sb.sync_log_id) AS min_sync_log_id
           FROM dwh_buffer.sync_buffer sb
          WHERE sb.processed IS NULL AND sb.table_name::text = 'ticket'::text))
 LIMIT 5000
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX mv_dwh_buffer_ticket_rowid_idx ON dwh_buffer.mv_dwh_buffer_ticket USING btree (rowid, sync_log_id);
CREATE INDEX mv_dwh_buffer_ticket_sync_log_id_idx ON dwh_buffer.mv_dwh_buffer_ticket USING btree (sync_log_id);


-- Permissions

ALTER TABLE dwh_buffer.mv_dwh_buffer_ticket OWNER TO dwh_agent;
GRANT ALL ON TABLE dwh_buffer.mv_dwh_buffer_ticket TO dwh_agent;

CREATE MATERIALIZED VIEW dwh_buffer.mv_dwh_buffer_ticket_entry
TABLESPACE pg_default
AS SELECT sync_buffer.sync_log_id,
    sync_buffer.table_row_id AS rowid,
    ((sync_buffer.data ->> 'subject'::text))::character varying(255) AS subject,
    btrim(regexp_replace(sync_buffer.data ->> 'body'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS body,
    (sync_buffer.data ->> 'ticket_id'::text)::bigint AS ticket_id,
    (sync_buffer.data ->> 'substatus_id'::text)::integer AS substatus_id,
    (sync_buffer.data ->> 'customer_safe'::text)::smallint AS customer_safe,
    (sync_buffer.data ->> 'created_by'::text)::integer AS created_by,
    (sync_buffer.data ->> 'created_on'::text)::timestamp without time zone AS created_on,
    (sync_buffer.data ->> 'last_updated_by'::text)::integer AS last_updated_by,
    (sync_buffer.data ->> 'last_updated_on'::text)::timestamp without time zone AS last_updated_on,
    ((sync_buffer.data ->> 'duration'::text))::character varying(255) AS duration,
    btrim(regexp_replace(sync_buffer.data ->> 'customer_note'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS customer_note,
    sync_buffer.data ->> 'time_spent'::character varying(255)::text AS time_spent,
    (sync_buffer.data ->> 'corrective_action'::text)::integer AS corrective_action,
    (sync_buffer.data ->> 'corrective_action_owner'::text)::integer AS corrective_action_owner,
    (sync_buffer.data ->> 'attachment1'::text)::integer AS attachment1,
    ((sync_buffer.data ->> 'sent_to'::text))::character varying(500) AS sent_to,
    ((sync_buffer.data ->> 'sent_cc'::text))::character varying(500) AS sent_cc,
    ((sync_buffer.data ->> 'sent_bcc'::text))::character varying(500) AS sent_bcc,
    (sync_buffer.data ->> 'sent_by'::text)::integer AS sent_by,
    (sync_buffer.data ->> 'sent_on'::text)::timestamp without time zone AS sent_on,
    btrim(regexp_replace(sync_buffer.data ->> 'email_subject'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS email_subject,
    (sync_buffer.data ->> 'email_type'::text)::integer AS email_type,
    (sync_buffer.data ->> 'forwarded_email_id'::text)::integer AS forwarded_email_id,
    (sync_buffer.data ->> 'incoming_email'::text)::integer AS incoming_email,
    ((sync_buffer.data ->> 'email_from'::text))::character varying(255) AS email_from,
    (sync_buffer.data ->> 'subject_id'::text)::integer AS subject_id,
    ((sync_buffer.data ->> 'sent_from'::text))::character varying(255) AS sent_from,
    (sync_buffer.data ->> 'service_review'::text)::smallint AS service_review,
    (sync_buffer.data ->> 'vendor_id'::text)::integer AS vendor_id,
    (sync_buffer.data ->> 'vendor_service_review_id'::text)::integer AS vendor_service_review_id,
    ((sync_buffer.data ->> 'email_header'::text))::character varying(500) AS email_header,
    (sync_buffer.data ->> 'is_manual_created'::text)::integer AS is_manual_created,
    (sync_buffer.data ->> 'legacy_note_id'::text)::integer AS legacy_note_id,
    (sync_buffer.data ->> 'ticket_entry_trigger_id'::text)::integer AS ticket_entry_trigger_id,
    (sync_buffer.data ->> 'noc_team_id'::text)::integer AS noc_team_id,
    ((sync_buffer.data ->> 'customer_name'::text))::character varying(255) AS customer_name,
    ((sync_buffer.data ->> 'customer_phone_numer'::text))::character varying(50) AS customer_phone_numer,
    (sync_buffer.data ->> 'caller_role_id'::text)::integer AS caller_role_id,
    ((sync_buffer.data ->> 'customer_email_address'::text))::character varying(255) AS customer_email_address,
    ((sync_buffer.data ->> 'technician_company'::text))::character varying(255) AS technician_company,
    (sync_buffer.data ->> 'is_new_issue'::text)::integer AS is_new_issue,
    (sync_buffer.data ->> 'is_escalation'::text)::integer AS is_escalation,
    (sync_buffer.data ->> 'service_experiencing_issue_id'::text)::integer AS service_experiencing_issue_id,
    ((sync_buffer.data ->> 'customer_fault_description'::text))::character varying(500) AS customer_fault_description,
    ((sync_buffer.data ->> 'gtt_managed_equipment_on_site_and_light_status'::text))::character varying(500) AS gtt_managed_equipment_on_site_and_light_status,
    (sync_buffer.data ->> 'is_site_hard_down'::text)::integer AS is_site_hard_down,
    (sync_buffer.data ->> 'has_remote_access_to_equipment'::text)::integer AS has_remote_access_to_equipment,
    (sync_buffer.data ->> 'is_light_status_captured'::text)::integer AS is_light_status_captured,
    (sync_buffer.data ->> 'is_power_verified'::text)::integer AS is_power_verified,
    (sync_buffer.data ->> 'is_cabling_verified'::text)::integer AS is_cabling_verified,
    (sync_buffer.data ->> 'is_site_level_impact_within_24_hours'::text)::integer AS is_site_level_impact_within_24_hours,
    ((sync_buffer.data ->> 'explenation'::text))::character varying(500) AS explenation,
    ((sync_buffer.data ->> 'pings_to_equipment'::text))::character varying(500) AS pings_to_equipment,
    ((sync_buffer.data ->> 'cal_example_date_time'::text))::character varying(500) AS cal_example_date_time,
    ((sync_buffer.data ->> 'originating_phone_number'::text))::character varying(255) AS originating_phone_number,
    ((sync_buffer.data ->> 'originating_destination_number'::text))::character varying(255) AS originating_destination_number,
    (sync_buffer.data ->> 'is_voice_call_example_available'::text)::integer AS is_voice_call_example_available,
    ((sync_buffer.data ->> 'ip_interface_brief'::text))::character varying(500) AS ip_interface_brief,
    ((sync_buffer.data ->> 'arp_table'::text))::character varying(500) AS arp_table,
    ((sync_buffer.data ->> 'routing_table'::text))::character varying(500) AS routing_table,
    ((sync_buffer.data ->> 'name_of_person_approving_escalation'::text))::character varying(255) AS name_of_person_approving_escalation,
    (sync_buffer.data ->> 'number_of_sites_impacted'::text)::integer AS number_of_sites_impacted,
    ((sync_buffer.data ->> 'troubleshooting_timeline'::text))::character varying(500) AS troubleshooting_timeline,
    ((sync_buffer.data ->> 'action_taken'::text))::character varying(500) AS action_taken,
    (sync_buffer.data ->> 'is_equipment_power_cycled'::text)::integer AS is_equipment_power_cycled,
    (sync_buffer.data ->> 'is_intervention'::text)::integer AS is_intervention,
    ((sync_buffer.data ->> 'caller_email_address'::text))::character varying(255) AS caller_email_address,
    ((sync_buffer.data ->> 'hours_of_availability'::text))::character varying(255) AS hours_of_availability,
    (sync_buffer.data ->> 'is_cam_note'::text)::integer AS is_cam_note,
    ((sync_buffer.data ->> 'intervention_to_string'::text))::character varying(500) AS intervention_to_string,
    ((sync_buffer.data ->> 'intervention_internal_emails'::text))::character varying(500) AS intervention_internal_emails,
    ((sync_buffer.data ->> 'decription_of_the_next_action'::text))::character varying(500) AS decription_of_the_next_action,
    (sync_buffer.data ->> 'date_of_the_next_action'::text)::timestamp without time zone AS date_of_the_next_action,
    ((sync_buffer.data ->> 'carrier_name'::text))::character varying(255) AS carrier_name,
    ((sync_buffer.data ->> 'carrier_contact_nunmber'::text))::character varying(255) AS carrier_contact_nunmber,
    ((sync_buffer.data ->> 'carrier_account_number'::text))::character varying(255) AS carrier_account_number,
    ((sync_buffer.data ->> 'issue_to_report'::text))::character varying(500) AS issue_to_report,
    ((sync_buffer.data ->> 'existing_ticket_number'::text))::character varying(255) AS existing_ticket_number,
    (sync_buffer.data ->> 'return_queue_id'::text)::integer AS return_queue_id,
    (sync_buffer.data ->> 'rma_shipment_id'::text)::integer AS rma_shipment_id,
    (sync_buffer.data ->> 'update_ticket_latest_status'::text)::integer AS update_ticket_latest_status,
    ((sync_buffer.data ->> 'hash'::text))::character varying(64) AS hash,
    (sync_buffer.data ->> 'inherited_from_master'::text)::integer AS inherited_from_master,
    ((sync_buffer.data ->> 'cost'::text))::numeric(28,6) AS cost,
    (sync_buffer.data ->> 'date_created'::text)::timestamp without time zone AS date_created,
    (sync_buffer.data ->> 'date_modified'::text)::timestamp without time zone AS date_modified,
    btrim(regexp_replace(sync_buffer.data ->> 'internal_note'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS internal_note
   FROM dwh_buffer.sync_buffer
  WHERE sync_buffer.table_name::text = 'ticket_entry'::text AND COALESCE(sync_buffer.processed::integer, 0) = 0 AND sync_buffer.sync_log_id = (( SELECT min(sb.sync_log_id) AS min
           FROM dwh_buffer.sync_buffer sb
          WHERE sb.processed IS NULL AND sb.table_name::text = 'ticket_entry'::text))
 LIMIT 5000
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX mv_dwh_buffer_ticket_entry_sync_log_id_idx ON dwh_buffer.mv_dwh_buffer_ticket_entry USING btree (sync_log_id, rowid);


-- Permissions

ALTER TABLE dwh_buffer.mv_dwh_buffer_ticket_entry OWNER TO dwh_agent;
GRANT ALL ON TABLE dwh_buffer.mv_dwh_buffer_ticket_entry TO dwh_agent;

CREATE MATERIALIZED VIEW dwh_buffer.mv_dwh_buffer_ticket_extra_fields
TABLESPACE pg_default
AS SELECT sync_buffer.sync_log_id,
    sync_buffer.table_row_id AS rowid,
    NULLIF(((sync_buffer.data ->> 'extra_fields'::text)::json) ->> 'icu'::text, ''::text)::timestamp without time zone AS icu,
        CASE
            WHEN (((sync_buffer.data ->> 'extra_fields'::text)::json) ->> 'icu'::text) IS NOT NULL AND (((sync_buffer.data ->> 'extra_fields'::text)::json) ->> 'icu'::text) <> ''::text THEN justify_interval(date_part('epoch'::text, now()::timestamp(0) without time zone::timestamp with time zone - to_timestamp(((sync_buffer.data ->> 'extra_fields'::text)::json) ->> 'icu'::text, 'YYYY-MM-DD HH24:MI:SS'::text)) * '00:00:01'::interval)::character varying(255)
            ELSE NULL::character varying
        END AS incoming_update
   FROM dwh_buffer.sync_buffer
  WHERE sync_buffer.table_name::text = 'ticket'::text AND COALESCE(sync_buffer.processed::integer, 0) = 0 AND sync_buffer.sync_log_id = (( SELECT min(sb.sync_log_id) AS min
           FROM dwh_buffer.sync_buffer sb
          WHERE sb.processed IS NULL AND sb.table_name::text = 'ticket'::text)) AND (sync_buffer.table_row_id IN ( SELECT mv_dwh_buffer_ticket.rowid
           FROM dwh_buffer.mv_dwh_buffer_ticket))
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX mv_dwh_buffer_ticket_extra_fields_sync_log_id_idx ON dwh_buffer.mv_dwh_buffer_ticket_extra_fields USING btree (sync_log_id, rowid);


-- Permissions

ALTER TABLE dwh_buffer.mv_dwh_buffer_ticket_extra_fields OWNER TO dwh_agent;
GRANT ALL ON TABLE dwh_buffer.mv_dwh_buffer_ticket_extra_fields TO dwh_agent;

CREATE MATERIALIZED VIEW dwh_buffer.mv_dwh_buffer_ticket_service
TABLESPACE pg_default
AS SELECT sync_buffer.sync_log_id,
    sync_buffer.table_row_id AS rowid,
    (sync_buffer.data ->> 'ticket_id'::text)::bigint AS ticket_id,
    (sync_buffer.data ->> 'created_on'::text)::timestamp without time zone AS created_on,
    (sync_buffer.data ->> 'deleted_on'::text)::timestamp without time zone AS date_created,
    (sync_buffer.data ->> 'product_id'::text)::bigint AS product_id
   FROM dwh_buffer.sync_buffer
  WHERE sync_buffer.table_name::text = 'ticket_service'::text AND COALESCE(sync_buffer.processed::integer, 0) = 0 AND sync_buffer.sync_log_id = (( SELECT min(sb.sync_log_id) AS min_sys_log_id
           FROM dwh_buffer.sync_buffer sb
          WHERE sb.processed IS NULL AND sb.table_name::text = 'ticket_service'::text))
 LIMIT 5000
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX mv_dwh_buffer_ticket_service_idx ON dwh_buffer.mv_dwh_buffer_ticket_service USING btree (rowid, sync_log_id);


-- Permissions

ALTER TABLE dwh_buffer.mv_dwh_buffer_ticket_service OWNER TO dwh_agent;
GRANT ALL ON TABLE dwh_buffer.mv_dwh_buffer_ticket_service TO dwh_agent;

CREATE MATERIALIZED VIEW dwh_buffer.mv_dwh_buffer_ticket_txt
TABLESPACE pg_default
AS SELECT sync_buffer.sync_log_id,
    sync_buffer.table_row_id AS rowid,
    ((sync_buffer.data ->> 'date_modified'::text))::character varying(50) AS date_modified,
    btrim(regexp_replace(sync_buffer.data ->> 'description'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS description,
    btrim(regexp_replace(sync_buffer.data ->> 'resolution'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS resolution,
    btrim(regexp_replace(sync_buffer.data ->> 'additional_info'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS additional_info,
    regexp_replace(sync_buffer.data ->> 'latest_status'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text)::character varying(400) AS latest_status,
    regexp_replace(sync_buffer.data ->> 'survey_not_sent'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text)::character varying(400) AS survey_not_sent,
    regexp_replace(sync_buffer.data ->> 'pw_location'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text)::character varying(400) AS pw_location,
    btrim(regexp_replace(sync_buffer.data ->> 'pw_reason'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS pw_reason,
    btrim(regexp_replace(sync_buffer.data ->> 'corrective_action'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS corrective_action,
    btrim(regexp_replace(sync_buffer.data ->> 'customer_resolution'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS customer_resolution,
    ((sync_buffer.data ->> 'es_speed'::text))::character varying(400) AS es_speed,
    ((sync_buffer.data ->> 'es_vlan'::text))::character varying(400) AS es_vlan,
    ((sync_buffer.data ->> 'ip_bs_route_result'::text))::character varying(400) AS ip_bs_route_result,
    ((sync_buffer.data ->> 'cc_pp_equipment'::text))::character varying(400) AS cc_pp_equipment,
    ((sync_buffer.data ->> 'cc_pp_alarms'::text))::character varying(400) AS cc_pp_alarms,
    ((sync_buffer.data ->> 'cc_pp_poc_details'::text))::character varying(400) AS cc_pp_poc_details,
    ((sync_buffer.data ->> 'a_demark_a'::text))::character varying(400) AS a_demark_a,
    ((sync_buffer.data ->> 'a_demark_z'::text))::character varying(400) AS a_demark_z,
    ((sync_buffer.data ->> 'z_demark_z'::text))::character varying(400) AS z_demark_z,
    ((sync_buffer.data ->> 'z_demark_a'::text))::character varying(400) AS z_demark_a,
    ((sync_buffer.data ->> 'a_loc_a'::text))::character varying(400) AS a_loc_a,
    ((sync_buffer.data ->> 'z_loc_z'::text))::character varying(400) AS z_loc_z,
    ((sync_buffer.data ->> 'interconnection_a'::text))::character varying(400) AS interconnection_a,
    ((sync_buffer.data ->> 'adhoc_test_1'::text))::character varying(400) AS adhoc_test_1,
    ((sync_buffer.data ->> 'adhoc_test_2'::text))::character varying(400) AS adhoc_test_2,
    ((sync_buffer.data ->> 'adhoc_test_3'::text))::character varying(400) AS adhoc_test_3,
    ((sync_buffer.data ->> 'comments'::text))::character varying(400) AS comments,
    ((sync_buffer.data ->> 'cc_pp_error_type'::text))::character varying(400) AS cc_pp_error_type,
    btrim(regexp_replace(sync_buffer.data ->> 'problem2'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS problem2,
    btrim(regexp_replace(sync_buffer.data ->> 'problem3'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS problem3,
    btrim(regexp_replace(sync_buffer.data ->> 'next_action'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS next_action,
    ((sync_buffer.data ->> 'ticket_sears_api_log'::text))::character varying(400) AS ticket_sears_api_log,
    ((sync_buffer.data ->> 'dispatch_tech_code'::text))::character varying(400) AS dispatch_tech_code,
    ((sync_buffer.data ->> 'dispatch_additional_notes'::text))::character varying(400) AS dispatch_additional_notes
   FROM dwh_buffer.sync_buffer
  WHERE sync_buffer.table_name::text = 'ticket'::text AND COALESCE(sync_buffer.processed::integer, 0) = 0 AND sync_buffer.sync_log_id = (( SELECT min(sb.sync_log_id) AS min
           FROM dwh_buffer.sync_buffer sb
          WHERE sb.processed IS NULL AND sb.table_name::text = 'ticket'::text)) AND (sync_buffer.table_row_id IN ( SELECT mv_dwh_buffer_ticket.rowid
           FROM dwh_buffer.mv_dwh_buffer_ticket))
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX mv_dwh_buffer_ticket_txt_sync_log_id_idx ON dwh_buffer.mv_dwh_buffer_ticket_txt USING btree (sync_log_id, rowid);


-- Permissions

ALTER TABLE dwh_buffer.mv_dwh_buffer_ticket_txt OWNER TO dwh_agent;
GRANT ALL ON TABLE dwh_buffer.mv_dwh_buffer_ticket_txt TO dwh_agent;

CREATE MATERIALIZED VIEW dwh_buffer.mv_dwh_buffer_vendor_ticket
TABLESPACE pg_default
AS SELECT sync_buffer.sync_log_id,
    sync_buffer.table_row_id AS rowid,
    ((sync_buffer.data ->> 'vendor_ticket'::text))::character varying(255) AS vendor_ticket,
    (sync_buffer.data ->> 'vendor_id'::text)::integer AS vendor_id,
    btrim(regexp_replace(sync_buffer.data ->> 'summary'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS summary,
    (sync_buffer.data ->> 'queue_id'::text)::integer AS queue_id,
    (sync_buffer.data ->> 'category_id'::text)::integer AS category_id,
    (sync_buffer.data ->> 'segment_id'::text)::integer AS segment_id,
    (sync_buffer.data ->> 'ticket_id'::text)::integer AS ticket_id,
    (sync_buffer.data ->> 'status_id'::text)::integer AS status_id,
    (sync_buffer.data ->> 'substatus_id'::text)::integer AS substatus_id,
    (sync_buffer.data ->> 'assigned_to_id'::text)::integer AS assigned_to_id,
    (sync_buffer.data ->> 'closure_responsibility_id'::text)::integer AS closure_responsibility_id,
    (sync_buffer.data ->> 'rfo_group_id'::text)::integer AS rfo_group_id,
    (sync_buffer.data ->> 'rfo_id'::text)::integer AS rfo_id,
    (sync_buffer.data ->> 'mttr'::text)::integer AS mttr,
    (sync_buffer.data ->> 'mttc'::text)::integer AS mttc,
    (sync_buffer.data ->> 'in_progress_on_date'::text)::timestamp without time zone AS in_progress_on_date,
    (sync_buffer.data ->> 'completed_on_date'::text)::timestamp without time zone AS completed_on_date,
    (sync_buffer.data ->> 'tracking_start_time'::text)::timestamp without time zone AS tracking_start_time,
    (sync_buffer.data ->> 'vendor_start_time'::text)::timestamp without time zone AS vendor_start_time,
    (sync_buffer.data ->> 'vendor_end_time'::text)::timestamp without time zone AS vendor_end_time,
    (sync_buffer.data ->> 'vendor_next_action_date'::text)::timestamp without time zone AS vendor_next_action_date,
    (sync_buffer.data ->> 'created_on'::text)::timestamp without time zone AS created_on,
    (sync_buffer.data ->> 'updated_on'::text)::timestamp without time zone AS updated_on,
    (sync_buffer.data ->> 'created_by'::text)::integer AS created_by,
    (sync_buffer.data ->> 'updated_by'::text)::integer AS updated_by,
    (sync_buffer.data ->> 'cancelled_on'::text)::timestamp without time zone AS cancelled_on,
    (sync_buffer.data ->> 'cancelled_by'::text)::integer AS cancelled_by,
    (sync_buffer.data ->> 'vendor_escalation_level_id'::text)::integer AS vendor_escalation_level_id,
    (sync_buffer.data ->> 'date_created'::text)::timestamp without time zone AS date_created,
    (sync_buffer.data ->> 'date_modified'::text)::timestamp without time zone AS date_modified,
    btrim(regexp_replace(sync_buffer.data ->> 'comments'::text, '[^a-zA-Z0-9/\r?\n|\r/_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text))::character varying(400) AS comments
   FROM dwh_buffer.sync_buffer
  WHERE sync_buffer.table_name::text = 'vendor_ticket'::text AND COALESCE(sync_buffer.processed::integer, 0) = 0 AND sync_buffer.sync_log_id = (( SELECT min(sb.sync_log_id) AS min
           FROM dwh_buffer.sync_buffer sb
          WHERE sb.processed IS NULL AND sb.table_name::text = 'vendor_ticket'::text))
 LIMIT 5000
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX mv_dwh_buffer_vendor_ticket_sync_log_id_idx ON dwh_buffer.mv_dwh_buffer_vendor_ticket USING btree (sync_log_id, rowid);


-- Permissions

ALTER TABLE dwh_buffer.mv_dwh_buffer_vendor_ticket OWNER TO dwh_agent;
GRANT ALL ON TABLE dwh_buffer.mv_dwh_buffer_vendor_ticket TO dwh_agent;

CREATE MATERIALIZED VIEW dwh_buffer.mv_dwh_buffer_wq_task
TABLESPACE pg_default
AS SELECT sync_buffer.sync_log_id,
    sync_buffer.table_row_id AS rowid,
    regexp_replace(sync_buffer.data ->> 'comments'::text, '[^a-zA-Z0-9/\r?\n|\r/_:_-_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text)::character varying(500) AS comments,
    (sync_buffer.data ->> 'wq_task_type_id'::text)::integer AS wq_task_type_id,
    (sync_buffer.data ->> 'record_id'::text)::integer AS record_id,
    (sync_buffer.data ->> 'status'::text)::integer AS status,
    (sync_buffer.data ->> 'assigned_to'::text)::integer AS assigned_to,
    (sync_buffer.data ->> 'start_date'::text)::timestamp without time zone AS start_date,
    (sync_buffer.data ->> 'due_date'::text)::timestamp without time zone AS due_date,
    (sync_buffer.data ->> 'scheduled_date'::text)::timestamp without time zone AS scheduled_date,
    (sync_buffer.data ->> 'created_by'::text)::integer AS created_by,
    (sync_buffer.data ->> 'created_on'::text)::timestamp without time zone AS created_on,
    (sync_buffer.data ->> 'updated_by'::text)::integer AS updated_by,
    (sync_buffer.data ->> 'updated_on'::text)::timestamp without time zone AS updated_on,
    (sync_buffer.data ->> 'preceding_task_id'::text)::integer AS preceding_task_id,
    regexp_replace(sync_buffer.data ->> 'summary'::text, '[^a-zA-Z0-9/\r?\n|\r/_:_-_._,_|_/_\_ ]+'::text, ' '::text, 'g'::text)::character varying(500) AS summary,
    (sync_buffer.data ->> 'completed_on'::text)::timestamp without time zone AS completed_on,
    (sync_buffer.data ->> 'canceled_date'::text)::timestamp without time zone AS canceled_date,
    (sync_buffer.data ->> 'reopened'::text)::integer AS reopened,
    (sync_buffer.data ->> 'sales_division_id'::text)::integer AS sales_division_id,
    (sync_buffer.data ->> 'ticket_id'::text)::integer AS ticket_id,
    (sync_buffer.data ->> 'escalate'::text)::integer AS escalate,
    (sync_buffer.data ->> 'escalate_label'::text)::character varying AS escalate_label,
    (sync_buffer.data ->> 'sof_id'::text)::integer AS sof_id,
    (sync_buffer.data ->> 'wq_task_type_criteria_id'::text)::integer AS wq_task_type_criteria_id,
    (sync_buffer.data ->> 'wq_task_queue_id'::text)::integer AS wq_task_queue_id,
    (sync_buffer.data ->> 'wq_task_category_id'::text)::integer AS wq_task_category_id,
    ((sync_buffer.data ->> 'reopened_label'::text))::character varying(255) AS reopened_label,
    (sync_buffer.data ->> 'substatus'::text)::integer AS substatus,
    (sync_buffer.data ->> 'disabled'::text)::integer AS disabled,
    (sync_buffer.data ->> 'date_modified'::text)::timestamp without time zone AS date_modified,
    (sync_buffer.data ->> 'date_created'::text)::timestamp without time zone AS date_created,
    (sync_buffer.data ->> 'ready_date'::text)::timestamp without time zone AS ready_date,
    (sync_buffer.data ->> 'sla_due_date'::text)::timestamp without time zone AS sla_due_date
   FROM dwh_buffer.sync_buffer
  WHERE sync_buffer.table_name::text = 'wq_task'::text AND COALESCE(sync_buffer.processed::integer, 0) = 0 AND sync_buffer.sync_log_id = (( SELECT min(sb.sync_log_id) AS min_sys_log_id
           FROM dwh_buffer.sync_buffer sb
          WHERE sb.processed IS NULL AND sb.table_name::text = 'wq_task'::text))
 LIMIT 5000
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX mv_dwh_buffer_wq_task_sync_log_id__unique_idx ON dwh_buffer.mv_dwh_buffer_wq_task USING btree (sync_log_id, rowid);
CREATE INDEX mv_dwh_buffer_wq_task_sync_log_id_idx ON dwh_buffer.mv_dwh_buffer_wq_task USING btree (sync_log_id);


-- Permissions

ALTER TABLE dwh_buffer.mv_dwh_buffer_wq_task OWNER TO dwh_agent;
GRANT ALL ON TABLE dwh_buffer.mv_dwh_buffer_wq_task TO dwh_agent;

CREATE OR REPLACE FUNCTION dwh_buffer.refresh_matviews_ticket()
 RETURNS void
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
REFRESH MATERIALIZED VIEW CONCURRENTLY dwh_buffer.mv_dwh_buffer_ticket with data;
REFRESH MATERIALIZED VIEW CONCURRENTLY dwh_buffer.mv_dwh_buffer_ticket_txt with data;
REFRESH MATERIALIZED VIEW CONCURRENTLY dwh_buffer.mv_dwh_buffer_ticket_extra_fields with data;
RETURN;
END;
$function$
;

-- Permissions

ALTER FUNCTION dwh_buffer.refresh_matviews_ticket() OWNER TO postgres;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket() TO public;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket() TO postgres;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket() TO dwh_agent;

CREATE OR REPLACE FUNCTION dwh_buffer.refresh_matviews_ticket_entry()
 RETURNS void
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
	REFRESH MATERIALIZED VIEW CONCURRENTLY dwh_buffer.mv_dwh_buffer_ticket_entry with data;
RETURN;
END;
$function$
;

-- Permissions

ALTER FUNCTION dwh_buffer.refresh_matviews_ticket_entry() OWNER TO postgres;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_entry() TO public;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_entry() TO postgres;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_entry() TO dwh_agent;

CREATE OR REPLACE FUNCTION dwh_buffer.refresh_matviews_ticket_extra_fields()
 RETURNS void
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
	REFRESH MATERIALIZED VIEW CONCURRENTLY dwh_buffer.mv_dwh_buffer_ticket_extra_fields with data;
RETURN;
END;
$function$
;

-- Permissions

ALTER FUNCTION dwh_buffer.refresh_matviews_ticket_extra_fields() OWNER TO dwh_agent;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_extra_fields() TO public;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_extra_fields() TO dwh_agent;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_extra_fields() TO postgres;

CREATE OR REPLACE FUNCTION dwh_buffer.refresh_matviews_ticket_service()
 RETURNS void
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
	REFRESH MATERIALIZED VIEW CONCURRENTLY dwh_buffer.mv_dwh_buffer_ticket_service with data;
RETURN;
END;
$function$
;

-- Permissions

ALTER FUNCTION dwh_buffer.refresh_matviews_ticket_service() OWNER TO postgres;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_service() TO public;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_service() TO postgres;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_ticket_service() TO dwh_agent;

CREATE OR REPLACE FUNCTION dwh_buffer.refresh_matviews_vendor_ticket()
 RETURNS void
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
	REFRESH MATERIALIZED VIEW CONCURRENTLY dwh_buffer.mv_dwh_buffer_vendor_ticket with data;
RETURN;
END;
$function$
;

-- Permissions

ALTER FUNCTION dwh_buffer.refresh_matviews_vendor_ticket() OWNER TO dwh_agent;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_vendor_ticket() TO dwh_agent;

CREATE OR REPLACE FUNCTION dwh_buffer.refresh_matviews_wq_task()
 RETURNS void
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
begin
	REFRESH MATERIALIZED VIEW CONCURRENTLY dwh_buffer.mv_dwh_buffer_wq_task with data;
RETURN;
END;
$function$
;

-- Permissions

ALTER FUNCTION dwh_buffer.refresh_matviews_wq_task() OWNER TO postgres;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_wq_task() TO public;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_wq_task() TO postgres;
GRANT ALL ON FUNCTION dwh_buffer.refresh_matviews_wq_task() TO dwh_agent;
