﻿get-date
$RootFolder="f:\FromSFTP_TEOCO_ORACLE_NEW\"
$FolderDestination=$RootFolder+"Loaded\"
$FileMaskSupplemental="*apiinvoice_details*.zip*"
$FolderExport=$RootFolder+"Export\"
$FolderArchive=$RootFolder+"Archived"
$SQL_SupplementalDetails=$RootFolder+"SQL_LoadSupplementalDetails.sql"
$SQL_ProcessMatches=$RootFolder+"SQL_ProcessMatches.sql"
$SQL_PostLoadStatusUpdate="SQL_PostLoadStatusUpdate.sql"
$DeleteMask=$FolderDestination+"*.csv"
$Supplemental_CSV="SupplementalDetails.csv"
#$OutputCmd='"SELECT * FROM [oracle].[CONSOLIDATED_INVOICE_LINES]" queryout ' + $OutputFile + ' -d TEOCO -t, -c -T'
#write-host($OutputCmd)


$CSVFiles = Get-ChildItem -Path $FolderDestination -Filter $FileMaskSupplemental |Sort-Object LastWriteTime
$CSVFiles | ForEach-Object { 
   $Filename=$_.FullName
   $Filename=$FolderDestination+$_
   write-host("======================================= SUPPLEMENTAL ============================================ Unzipping...")

   tar -xvzf $Filename -C $FolderDestination

   Get-ChildItem -Path $FolderDestination -Filter *apiinvoice_details.csv | Foreach-Object{   
     $NewName = $Supplemental_CSV
     Write-Host $NewName
     Rename-Item -Path $_.FullName -NewName $NewName
   }
 
   # SQL games
   # Data import
   sqlcmd -i $SQL_SupplementalDetails -v varCSVName=$Supplemental_CSV -v varZIPName=$_ 

   write-host("Removing CSV: ",$DeleteMask)
   Remove-item -Path $DeleteMask 
   write-host("Moving ZIP: ",$_)
   Move-Item -Path $_.FullName -Destination $FolderArchive -Force
}
get-childItem $FolderExport* | where {$_.length -eq 0} | remove-Item
get-date
