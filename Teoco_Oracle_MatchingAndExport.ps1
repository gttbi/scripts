﻿write-host(">>>>>>>>>>>>>>>> Starting the matching process >>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
get-date

$RootFolder="f:\FromSFTP_TEOCO_ORACLE_NEW\"
$FolderDestination=$RootFolder+"Loaded\"
$FolderExport=$RootFolder+"Export\"
$FolderArchive=$RootFolder+"Archived"

$SQL_ProcessMatches=$RootFolder+"SQL_ProcessMatches.sql"

sqlcmd -i $SQL_ProcessMatches
get-date
write-host("<<<<<<<<<<<<<<<< Finish of the matching process >>>>>>>>>>>>>>>>>>>>>>>>>>>>")
