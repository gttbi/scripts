USE TEOCO;

DECLARE @DIRECTORY varchar(255) = 'f:\FromSFTP_TEOCO_ORACLE_NEW\Loaded\';
DECLARE @FILE_NAME varchar(255) = N'$(varCSVName)';
DECLARE @FILE_ZIP_NAME varchar(255) = N'$(varZIPName)';
DECLARE @FULL_NAME varchar(255) = @DIRECTORY + @FILE_NAME;
DECLARE @SQL_CMD varchar(5000);
DECLARE @FIELDTERMINATOR varchar(10) =',', @ROWTERMINATOR varchar(10)='\n', @FIRSTROW char(1)= '2';
DECLARE @ErrorMsg varchar(1000) =  '!!!!!!!!!!!!!!!!!! The file ' + @FILE_ZIP_NAME + ' was previously imported with invoice lines !!!!!!!!!!!!!!!!!!!!!!!!!!!';


declare @stmt varchar(max);
declare @load_id bigint = -1, @load_group_id bigint = -1, @recordcount bigint = 0;

:ON ERROR EXIT

--# create a new record for the LOAD_GROUP and set to Loading
INSERT INTO oracle.LOAD_GROUP (LOAD_GROUP_STATUS, EXPORT_FILE) VALUES('Loading',NULL)
SET @load_group_id = (SELECT IDENT_CURRENT('oracle.LOAD_GROUP'));

--# create a new record for the LOAD_CYCLE
INSERT INTO oracle.LOAD_CYCLE  WITH (TABLOCKX) VALUES(@load_group_id, getdate(),@FILE_ZIP_NAME,'oracle.TMP_SUPPLEMENTAL_DETAILS',NULL,NULL);
SET @load_id = (SELECT IDENT_CURRENT('oracle.LOAD_CYCLE'));
print 'Determined Supplemental LOAD_CYCLE_ID:' + CAST(@load_id as varchar(50));

TRUNCATE TABLE [oracle].TMP_SUPPLEMENTAL_DETAILS;

set @SQL_CMD = 'BULK INSERT [oracle].[TMP_SUPPLEMENTAL_DETAILS] FROM ''' + @FULL_NAME + ''' WITH (FORMAT = ''CSV'',KEEPNULLS, DATAFILETYPE=''char'', FIELDQUOTE = ''"'', FIELDTERMINATOR ='''+ @FIELDTERMINATOR + ''',ROWTERMINATOR =''' + @ROWTERMINATOR + ''',FirstRow=' + @FIRSTROW + ')';
PRINT @SQL_CMD;
exec (@SQL_CMD);

-- deleting all what already exists
DELETE FROM oracle.[SUPPLEMENTAL_DETAILS] WHERE [TRANSACTION_HEADER_ID] In (SELECT [TRANSACTION_HEADER_ID] FROM oracle.TMP_SUPPLEMENTAL_DETAILS)
PRINT 'DELETION OF THE OLD SUPPLEMENTAL RECORDS IS DONE'

-- inserting all new, including previously deleted
INSERT INTO [oracle].[SUPPLEMENTAL_DETAILS]
           ([LOAD_ID]
           ,[TRANSACTION_HEADER_ID]
           ,[BUSINESS_UNIT]
           ,[INVOICE]
           ,[BILL_DATE]
           ,[DUE_DATE]
           ,[VENDOR]
           ,[VENDOR_LOC_CODE]
           ,[REMIT_ADDRESS]
           ,[CURRENCY]
           ,[LEGAL_ENTITY]
           ,[BAN]
           ,[BILLING_FREQUENCY_DESC]
           ,[CHARGE_TYPE]
           ,[AMOUNT]
           ,[MRC_LINE_ITEM_CODE_TEXT]
           ,[OCC_LINE_ITEM_CODE_TEXT]
           ,[PURCHASE_ORDER_NUMBER]
           ,[STRIP_EC_CIRCUIT_ID]
           ,[INTERNAL_CIRCUIT_ID]
           ,[OCL]
           ,[WTN]
           ,[SERVICE_ORDER_NUMBER]
           ,[OCC_CIRCUIT_DESCRIPTION]
           ,[MRC_CIRCUIT_DESCRIPTION]
           ,[MRC_CIRCUIT_LOCATION_ADDRESS]
           ,[OCC_CIRCUIT_LOCATION_ADDRESS]
           ,[FROM_DATE]
           ,[THRU_DATE]
           ,[TRANSACTION_WORKFLOW_STATE]
           ,[INVOICE_WORKFLOW_STATE]
           ,[ACCOUNT_CODE_STRING]
           ,[ACCOUNTING_DATE]
           ,[LOGGED_DATE]
           ,[SLID]
           ,[SEGMENT_ID]
           ,[CUSTOMER_NAME])

SELECT 
       @load_id,
       [TRANSACTION_HEADER_ID]
      ,[BUSINESS_UNIT]
      ,[INVOICE]
      ,[BILL_DATE]
      ,[DUE_DATE]
      ,[VENDOR]
      ,[VENDOR_LOC_CODE]
      ,[REMIT_ADDRESS]
      ,[CURRENCY]
      ,[LEGAL_ENTITY]
      ,[BAN]
      ,[BILLING_FREQUENCY_DESC]
      ,[CHARGE_TYPE]
      ,[AMOUNT]
      ,[MRC_LINE_ITEM_CODE_TEXT]
      ,[OCC_LINE_ITEM_CODE_TEXT]
      ,[PURCHASE_ORDER_NUMBER]
      ,[STRIP_EC_CIRCUIT_ID]
      ,[INTERNAL_CIRCUIT_ID]
      ,[OCL]
      ,[WTN]
      ,[SERVICE_ORDER_NUMBER]
      ,[OCC_CIRCUIT_DESCRIPTION]
      ,[MRC_CIRCUIT_DESCRIPTION]
      ,[MRC_CIRCUIT_LOCATION_ADDRESS]
      ,[OCC_CIRCUIT_LOCATION_ADDRESS]
      ,[FROM_DATE]
      ,[THRU_DATE]
      ,[TRANSACTION_WORKFLOW_STATE]
      ,[INVOICE_WORKFLOW_STATE]
      ,[ACCOUNT_CODE_STRING]
      ,[ACCOUNTING_DATE]
      ,[LOGGED_DATE]
      ,[SLID]
      ,[SEGMENT_ID]
      ,[CUSTOMER_NAME]
  FROM [oracle].[TMP_SUPPLEMENTAL_DETAILS]

  SET @recordcount = @@ROWCOUNT;
  IF @recordcount > 0 BEGIN
	PRINT 'INSERTION from ZIP file ' + @FILE_ZIP_NAME + ' to [oracle].[SUPPLEMENTAL_DETAILS] completed';
	UPDATE oracle.LOAD_CYCLE SET LOAD_SUCCESS=1, LOADED_ROWS=@recordcount WHERE LOAD_ID = @load_id
	UPDATE oracle.LOAD_GROUP SET LOAD_GROUP_STATUS='Loaded' WHERE LOAD_GROUP_ID = (SELECT LOAD_GROUP_ID FROM oracle.LOAD_CYCLE WHERE LOAD_ID=@load_id)
  END ELSE BEGIN
	PRINT '   !!!!!!!!!!!!!!!! INSERTION from ZIP file ' + @FILE_ZIP_NAME + ' to [oracle].[SUPPLEMENTAL_DETAILS] WAS FAILED !!!!!!!!!!!!!!!!';
	UPDATE oracle.LOAD_CYCLE SET LOAD_SUCCESS=0, LOADED_ROWS=@recordcount WHERE LOAD_ID = @load_id
	UPDATE oracle.LOAD_GROUP SET LOAD_GROUP_STATUS='Failed' WHERE LOAD_GROUP_ID = (SELECT LOAD_GROUP_ID FROM oracle.LOAD_CYCLE WHERE LOAD_ID=@load_id)
  END

