﻿get-date
$RootFolder="f:\FromSFTP_TEOCO_ORACLE_NEW\"
$FolderDestination=$RootFolder+"Loaded\"
$FileMaskSupplemental="*apiinvoice_details*.zip*"
$FolderExport=$RootFolder+"Export\"
$FolderArchive=$RootFolder+"Archived"
$SQL_SupplementalDetails=$RootFolder+"SQL_LoadSupplementalDetails.sql"
$SQL_ProcessMatches=$RootFolder+"SQL_ProcessMatches.sql"
$SQL_PostLoadStatusUpdate="SQL_PostLoadStatusUpdate.sql"
$DeleteMask=$FolderDestination+"*.csv"
$Supplemental_CSV="SupplementalDetails.csv"
#$OutputCmd='"SELECT * FROM [oracle].[CONSOLIDATED_INVOICE_LINES]" queryout ' + $OutputFile + ' -d TEOCO -t, -c -T'
#write-host($OutputCmd)


$CSVFiles = Get-ChildItem -Path $FolderDestination -Filter $FileMaskSupplemental |Sort-Object LastWriteTime
$CSVFiles | ForEach-Object { 
   $Filename=$_.FullName
   write-host("GOOD FILE! `n")
   $Filename=$FolderDestination+$_
   write-host("Unzipping...")
   tar -xvzf $Filename -C $FolderDestination

   Get-ChildItem -Path $FolderDestination -Filter *apiinvoice_details.csv | Foreach-Object{   
     $NewName = $Supplemental_CSV
     Write-Host $NewName
     Rename-Item -Path $_.FullName -NewName $NewName
   }
 
   # SQL games
   # Data import
   sqlcmd -i $SQL_SupplementalDetails -v varCSVName=$Supplemental_CSV -v varZIPName=$_ 
   
   #Mark the exported LOAD_CYCLE_ID as PROCESSED
   sqlcmd -i $SQL_ProcessMatches -v varZIPName=$_
   #Export
   #Export to CSV - parameters
   $OutputFile=$RootFolder+"Export\oracle_output_$(get-date -f yyyy-MM-dd_HH-MM-ss).csv"
   $Query='"SELECT * FROM [oracle].[CONSOLIDATED_INVOICE_LINES_AGGR] WHERE LOAD_GROUP_ID = (SELECT MIN(LOAD_GROUP_ID) FROM oracle.LOAD_GROUP WHERE LOAD_GROUP_STATUS=''Processed'')"'
   $bcp = '"c:\Program Files\Microsoft SQL Server\110\Tools\Binn\bcp.exe"'
   $arglist = @(
	$Query,
	"queryout $OutputFile",
	"-S localhost",
	"-d TEOCO",
	"-t, -c -T"
   )

   write-host("$bcp $arglist")
   Start-Process -FilePath $bcp -ArgumentList $arglist

   #Start-Sleep 3
   #Set the status to Exported
   sqlcmd -i $SQL_PostLoadStatusUpdate

   write-host("Removing CSV: ",$DeleteMask)
   Remove-item -Path $DeleteMask 
   write-host("Moving ZIP: ",$_)
   Move-Item -Path $_.FullName -Destination $FolderArchive -Force
}
get-childItem $FolderExport* | where {$_.length -eq 0} | remove-Item
get-date
