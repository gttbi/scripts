$RootFolder="f:\FromSFTP_TEOCO_ORACLE_NEW\"
$FolderDestination=$RootFolder+"Loaded\"
$FolderArchive=$RootFolder+"Archived"
$InvoiceLines_SQL=$RootFolder+"InvoiceLines.sql"
$InvoiceHeaders_SQL=$RootFolder+"InvoiceHeaders.sql"
$Processing_SQL=$RootFolder+"Processing.sql"
$DeleteMask=$FolderDestination+"*.csv"
$SupplementalLines_SQL=$RootFolder+"SupplementalLines.sql"
$SupplementalDetails_CSV=$RootFolder+"SupplementalDetails.csv" 

#ls f:\FromSFTP_TEOCO_ORACLE_NEW\GTT_apiinvoice_details.csv| %{Rename-Item $_ -NewName ("SupplementalDetails.csv" -f $nr++)}

#ls f:\FromSFTP_TEOCO_ORACLE_NEW\GTT_apiinvoice_details*.csv| %{Rename-Item $_ -NewName ("SupplementalDetails.csv" -f $nr++)}

Get-ChildItem -Path $FolderDestination -Filter *.csv | Foreach-Object{   
   $NewName = $_.Name -replace "ABC_(.*?)\d{2}_\d{4}.xml", "XYZ_`$1.xml"
   $NewName = "SupplementalDetails.csv"
   Write-Host $NewName
   Rename-Item -Path $_.FullName -NewName $NewName
}

#   Dir *apiinvoice_details.csv | ForEach-Object  -begin { $count=1 }  -process { rename-item $_ -NewName $SupplementalDetails_CSV; $count++ }
