@echo off

set mydate=%date:~10,4%%date:~4,2%%date:~7,2%
set local_root=f:\FromSFTP_TEOCO_ORACLE_NEW
set local_dest=%local_root%\Loaded
set local_archived=%local_root%\Archived
set local_filelist=%local_root%\file_list.txt
set local_ps_log=%local_root%\ps_log_%mydate%.log
set local_sql_log=_sql_log%mydate%.log


for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"

set "datestamp=%YYYY%%MM%%DD%" & set "timestamp=%HH%%Min%%Sec%"
set "fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%"
REM echo datestamp: "%datestamp%"
REM echo timestamp: "%timestamp%"
echo =============================================================================================>>%local_ps_log%
echo Load datetime: "%fullstamp%">>%local_ps_log%

set mydate=%date:~10,4%%date:~4,2%%date:~7,2%
@echo on

del *.log /Q

echo =============================================================================================>>%local_sql_log%
powershell ".\Teoco_Oracle_DATA_TRUNCATE.ps1" >> %local_sql_log%
powershell ".\Teoco_Oracle_CSVtoDB_HeadersAndLines.ps1" >> %local_sql_log%
powershell ".\Teoco_Oracle_CSVtoDB_Supplemental_NO_ZIP.ps1" >> %local_sql_log%
powershell ".\Teoco_Oracle_MatchingAndExport.ps1" >> %local_sql_log%

@ECHO OFF
rem bcp "SELECT * FROM [oracle].[CONSOLIDATED_INVOICE_LINES]" queryout %local_root%\Export\invoice_lines_consolidated.csv -d TEOCO -t, -c -T
rem move %local_dest%\*.* %local_archived%