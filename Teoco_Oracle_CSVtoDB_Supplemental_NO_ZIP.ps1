﻿get-date
$RootFolder="f:\FromSFTP_TEOCO_ORACLE_NEW\"
$FolderDestination=$RootFolder+"Loaded\"
$FileMaskSupplemental="**apinvoiceimport_detail*.csv*"
$FolderExport=$RootFolder+"Export\"
$FolderArchive=$RootFolder+"Archived"
$FolderUpload=$RootFolder+"Upload"

$SQL_SupplementalDetails=$RootFolder+"SQL_LoadSupplementalDetails.sql"
$SQL_ProcessMatches=$RootFolder+"SQL_ProcessMatches.sql"
$SQL_PostLoadStatusUpdate="SQL_PostLoadStatusUpdate.sql"
$DeleteMask=$FolderDestination+"*.csv"
$Supplemental_CSV="SupplementalDetails.csv"
#$OutputCmd='"SELECT * FROM [oracle].[CONSOLIDATED_INVOICE_LINES]" queryout ' + $OutputFile + ' -d TEOCO -t, -c -T'
#write-host($OutputCmd)



write-host($FolderDestination+$FileMaskSupplemental)


$CSVFiles = Get-ChildItem -Path $FolderDestination -Filter $FileMaskSupplemental |Sort-Object LastWriteTime -Descending
$CSVFiles | ForEach-Object { 
   $Filename=$_.FullName
   $Filename=$FolderDestination+$_
   write-host("======================================= SUPPLEMENTAL CSV ============================================")


   # SQL games
   # Data import
   sqlcmd -i $SQL_SupplementalDetails -v varCSVName=$_ -v varZIPName=$_ 

   write-host("Copying ZIP to Upload: ",$_)
   Copy-Item -Path $_.Fullname -Destination $FolderUpload

   write-host("Moving CSV to Archived: ",$_)
   Move-Item -Path $_.FullName -Destination $FolderArchive -Force
}
get-date
