USE TEOCO;
DECLARE @load_group_id bigint;
--DECLARE @last_header_load_group_id bigint = N'$(varHeader_LOAD_GROUP_ID)';

PRINT 'Postprocessing started';

 IF EXISTS(SELECT LOAD_GROUP_ID FROM oracle.LOAD_GROUP WHERE LOAD_GROUP_STATUS = 'Loaded') BEGIN
	UPDATE oracle.LOAD_GROUP
		SET LOAD_GROUP_STATUS = 'Exported'
	WHERE LOAD_GROUP_STATUS = 'Processed';
 END

