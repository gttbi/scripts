@echo on

set mydate=%date:~10,4%%date:~4,2%%date:~7,2%
set local_root=f:\FromSFTP_TEOCO_ORACLE_NEW
set local_dest=%local_root%\Loaded
set local_export=%local_root%\Export
set local_export_mask=*.csv
set local_upload=%local_root%\Upload

set local_archived=%local_root%\Archived

set local_filelist=%local_root%\file_list.txt

set local_ps_log=%local_root%\_ps_log_%mydate%.log
set local_sql_log=_sql_log%mydate%.log


for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"

set "datestamp=%YYYY%%MM%%DD%" & set "timestamp=%HH%%Min%%Sec%"
set "fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%"


set mydate=%date:~10,4%%date:~4,2%%date:~7,2%

echo FILE ARCHIVING SCRIPT
powershell.exe -File %local_root%\ArchiveNewFiles.ps1 -sourcePath %local_export% -uploadPath %local_upload% -exportMask %local_export_mask%>>%local_ps_log%

echo FILE UPLOADING SCRIPT
winscp.com sftp://oraclesftp@10.150.22.55/test/teoco/processed/ -hostkey="ssh-ed25519 255 jP0WfF4+c3sif8OFxpqMFZeB8O5IMTSIHJakoWZ3snU=" -privatekey="C:\CERTIFICATE\nyprbidwh_dw_sftp.ppk" /script=%local_root%\UploadNewFiles.txt /log=%local_root%\_log_sftp_scr%mydate%.log


