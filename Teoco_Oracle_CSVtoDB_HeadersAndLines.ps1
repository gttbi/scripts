﻿param ($sourcePath, $uploadPath, $archivePath, $archiveMask)
write-host("........................")
write-host("Passed parameters:")
write-host($sourcePath)
write-host($uploadPath)
write-host($archivePath)
write-host($archiveMask)
write-host("........................")

get-date
$DeleteMask=$sourcePath+"\*ApInvoice*Interface.csv"

$InvoiceLines_SQL=$RootFolder+"SQL_LoadInvoiceLines.sql"
$InvoiceHeaders_SQL=$RootFolder+"SQL_LoadInvoiceHeaders.sql"
$SQL_HeaderLoadStatusUpdate="SQL_HeaderLoadStatusUpdate.sql"


$CSVFiles = Get-ChildItem -Path $sourcePath -Filter *apinvoiceimport.zip* |Sort-Object LastWriteTime -Descending
$CSVFiles | ForEach-Object { 
   $_.FullName
   write-host("GOOD FILE! `n")
   $Filename=$sourcePath+"\"+$_
   write-host("Unzipping...")
   tar -xvzf $Filename -C $sourcePath\
   # SQL games
   # Data import and setting the LOAD_GROUP_ID as 'Ready'
   sqlcmd -i $InvoiceHeaders_SQL -v varCSVName=ApInvoicesInterface.csv -v varZIPName=$_
   sqlcmd -i $InvoiceLines_SQL -v varCSVName=ApInvoiceLinesInterface.csv -v varZIPName=$_ 
   sqlcmd -i $SQL_HeaderLoadStatusUpdate -v varZIPName=$_ 

   #Start-Sleep 3

   write-host("Removing CSV: ",$DeleteMask)
   Remove-item -Path $DeleteMask 

   write-host("Copying ZIP: ",$_)
   Copy-Item -Path $_.Fullname -Destination $uploadPath

   write-host("Moving ZIP: ",$_)
   write-host("to: ",$archivePath)

   Move-Item -Path $_.FullName -Destination $archivePath -Force
}
get-date
