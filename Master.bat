@echo off

set mydate=%date:~10,4%%date:~4,2%%date:~7,2%
set local_root=f:\FromSFTP_TEOCO_ORACLE_NEW
set local_dest=%local_root%\Loaded

rem this a the calling parameters
set local_loaded_path=%local_root%\Loaded 
set local_archive_path=%local_root%\Archived
set local_upload_path=%local_root%\Upload
set archiveMask=*.zip



set local_archived=%local_root%\Archived

set local_filelist=%local_root%\file_list.txt

# debug mode = delete log files and the filelist
rem del %local_filelist% /Q
rem del *.log /Q

set local_ps_log=%local_root%\_ps_log_%mydate%.log
set local_sql_log=_sql_log%mydate%.log


for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"

set "datestamp=%YYYY%%MM%%DD%" & set "timestamp=%HH%%Min%%Sec%"
set "fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%"
REM echo datestamp: "%datestamp%"
REM echo timestamp: "%timestamp%"
echo =============================================================================================>>%local_ps_log%
echo Load datetime: "%fullstamp%">>%local_ps_log%

set mydate=%date:~10,4%%date:~4,2%%date:~7,2%
@echo on

rem set myurl=sftp://oraclesftp;fingerprint=ssh-ed25519-jP0WfF4-c3sif8OFxpqMFZeB8O5IMTSIHJakoWZ3snU;x-name=dw_sftp%4010.150.22.55;x-publickeyfile=C:\CERTIFICATE\nyprbidwh_dw_sftp.ppk@10.150.22.55/
echo Connecting to TEOCO FTP...
set myurl=sftp://gtt-sftp:oDDXizwI;fingerprint=ssh-rsa--AvwiHp-z9UaBNrN5YQ44sF2cYUEqHXyp6aKaXfHFf8@ftp.teocosolutions.com/
cd %local_root%

set remote_fileMask="GTT_*apinvoiceimport.zip*"
powershell.exe -File %local_root%\LoadNewFiles.ps1 -sessionUrl %myurl% -localPath %local_dest% -remotePath "/FromTEOCO/AP" -listPath %local_filelist% -fileMask %remote_fileMask%>>%local_ps_log%

set remote_fileMask="GTT_apinvoiceimport_detail*.csv"
powershell.exe -File %local_root%\LoadNewFiles.ps1 -sessionUrl %myurl% -localPath %local_dest% -remotePath "/FromTEOCO/AP" -listPath %local_filelist% -fileMask %remote_fileMask%>>%local_ps_log%


echo =============================================================================================>>%local_sql_log%
echo =============================================================================================>>%local_sql_log%

rem powershell ".\Teoco_Oracle_DATA_TRUNCATE.ps1" >> %local_sql_log%


powershell ".\Teoco_Oracle_CSVtoDB_HeadersAndLines.ps1" -sourcePath %local_loaded_path% -uploadPath %local_upload_path% -archivePath %local_archive_path% -archiveMask %archiveMask%>> %local_sql_log%
powershell ".\Teoco_Oracle_CSVtoDB_Supplemental_NO_ZIP.ps1" >> %local_sql_log%
powershell ".\Teoco_Oracle_MatchingAndExport.ps1" >> %local_sql_log%

@ECHO OFF
rem bcp "SELECT * FROM [oracle].[CONSOLIDATED_INVOICE_LINES]" queryout %local_root%\Export\invoice_lines_consolidated.csv -d TEOCO -t, -c -T
rem move %local_dest%\*.* %local_archived%



@echo on

set mydate=%date:~10,4%%date:~4,2%%date:~7,2%
set local_root=f:\FromSFTP_TEOCO_ORACLE_NEW
set local_export=%local_root%\Export
set local_export_mask=*.csv
set local_upload=%local_root%\Upload

rem echo FILE ARCHIVING SCRIPT
rem powershell.exe -File %local_root%\ArchiveNewFiles.ps1 -sourcePath %local_export% -uploadPath %local_upload% -exportMask %local_export_mask%>>%local_ps_log%

echo FILE UPLOADING SCRIPT
winscp.com sftp://oraclesftp@10.150.22.55/test/teoco/processed/ -hostkey="ssh-ed25519 255 jP0WfF4+c3sif8OFxpqMFZeB8O5IMTSIHJakoWZ3snU=" -privatekey="C:\CERTIFICATE\nyprbidwh_dw_sftp.ppk" /script=%local_root%\UploadNewFiles.txt /log=%local_root%\_log_sftp_scr%mydate%.log

move /y %local_upload%\*.zip %local_archive_path%
move /y %local_upload%\*.csv %local_archive_path%

rem del %local_upload%\*.zip /Q
rem del %local_upload%\*.csv /Q


