﻿get-date
$RootFolder="f:\FromSFTP_TEOCO_ORACLE_NEW\"
$FolderDestination=$RootFolder+"Loaded\"
$FileMask="*apinvoiceimport.zip*"
$FolderExport=$RootFolder+"Export\"
$FolderArchive=$RootFolder+"Archived"
$InvoiceLines_SQL=$RootFolder+"SQL_LoadInvoiceLines.sql"
$InvoiceHeaders_SQL=$RootFolder+"SQL_LoadInvoiceHeaders.sql"
$DeleteMask=$FolderDestination+"*ApInvoice*.csv"
$SQL_HeaderLoadStatusUpdate="SQL_HeaderLoadStatusUpdate.sql"
#$OutputCmd='"SELECT * FROM [oracle].[CONSOLIDATED_INVOICE_LINES]" queryout ' + $OutputFile + ' -d TEOCO -t, -c -T'
#write-host($OutputCmd)
$SQL_DATA_TRUNCATE=$RootFolder+"SQL_DATA_TRUNCATE.sql"

sqlcmd -i $SQL_DATA_TRUNCATE
